import React from "react";
import styled from "react-emotion";

import {
  BlockQuote,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Slide,
  Text,
  Appear
} from "spectacle";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Particles
import ParticleField from "react-particles-webgl";
const config = {
  showCube: true,
  dimension: "3D",
  velocity: 2,
  lines: {
    colorMode: "rainbow",
    color: "#351CCB",
    transparency: 0.9,
    limitConnections: true,
    maxConnections: 20,
    minDistance: 150,
    visible: true
  },
  particles: {
    colorMode: "",
    color: "#3FB568",
    transparency: 0.9,
    shape: "square",
    count: 500,
    minSize: 110,
    maxSize: 175,
    visible: true
  },
  cameraControls: {
    enabled: true,
    enableDamping: true,
    dampingFactor: 0.2,
    enableZoom: true,
    autoRotate: true,
    autoRotateSpeed: 0.7,
    resetCameraFlag: false
  }
};

require("normalize.css");

const theme = createTheme(
  {
    primary: "#2e2e2e",
    secondary: "#ddffab",
    tertiary: "#f16a70",
    quaternary: "#8cdcda"
  },
  {
    primary: {
      name: "Comfortaa",
      googleFont: true
    },
    secondary: "Ubuntu"
  }
);

const CustomListItem = styled(ListItem)`
  font-size: 1.1em;
  line-height: 1.2em;
  margin-bottom: 20px;

  &:last-child {
    margin-bottom: 0;
  }
`;

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={["zoom", "slide"]}
        transitionDuration={500}
        theme={theme}
      >
        <Slide transition={["zoom"]}>
          <ParticleField config={config} />
          <Heading caps lineHeight={1.2} textColor="secondary" font="secondary">
            Ideathon
          </Heading>

          <Text size={6} textColor="quaternary">
            Wrocław, 27-28.03.2019
          </Text>
        </Slide>

        <Slide transition={["fade"]} bgColor="primary">
          <Heading size={4} textColor="tertiary" caps>
            <span role="img" aria-label="emoji">
              💡
            </span>{" "}
            Idea generators:
          </Heading>
          <List textColor="secondary">
            <CustomListItem>Dominika Podgórska</CustomListItem>
            <CustomListItem>Grzegorz Herbowicz</CustomListItem>
            <CustomListItem>Tadeusz Ćwichuła</CustomListItem>
          </List>
        </Slide>

        <Slide transition={["spin"]}>
          <Heading size={4} textColor="tertiary" caps>
            Problem Statement
          </Heading>
          <List>
            <Appear>
              <CustomListItem>
                Hard to get a help from other employees without knowing what
                they are good at and who should you contact with{" "}
                <span role="img" aria-label="emoji">
                  🤔
                </span>
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Current solution (scorecards) are not allowing managers to
                search{" "}
                <span role="img" aria-label="emoji">
                  🔎
                </span>{" "}
                employees by skills, experience, et cætera
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Lack of information about currently available Adidas projects{" "}
                <span role="img" aria-label="emoji">
                  🤷
                </span>
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Prolonged waiting time on „bench” and lengthy onboarding period{" "}
                <span role="img" aria-label="emoji">
                  💤
                </span>
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Projects not fitting the skills described in a scorecard by
                analysts & consultants{" "}
                <span role="img" aria-label="emoji">
                  🤦
                </span>
              </CustomListItem>
            </Appear>
          </List>
        </Slide>

        <Slide transition={["zoom"]} bgColor="primary" textColor="secondary">
          <Heading size={4} textColor="tertiary" caps>
            Proposed Solution
          </Heading>
          <List>
            <Appear>
              <CustomListItem>
                Internal application facilitating the process of matching{" "}
                <span role="img" aria-label="emoji">
                  🎯
                </span>{" "}
                employees to Adidas projects
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Dedicated software for managing employees across multiple
                projects, scheduling the recruitment process from end to end{" "}
                <span role="img" aria-label="emoji">
                  👌
                </span>
                <span role="img" aria-label="emoji">
                  🏆
                </span>
              </CustomListItem>
            </Appear>
          </List>
        </Slide>

        <Slide transition={["slide"]} bgColor="primary" textColor="secondary">
          <Heading size={4} textColor="tertiary" caps>
            Solution Description
          </Heading>
          <List>
            <Appear>
              <CustomListItem>
                <span role="img" aria-label="emoji">
                  👥
                </span>{" "}
                Employees have profiles including contact details, skills
                listed, projects completed, people worked with (like scorecard)
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Managers provide{" "}
                <span role="img" aria-label="emoji">
                  💬
                </span>{" "}
                information about currently available projects and near future
                plans with technical requirements
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Employees can search{" "}
                <span role="img" aria-label="emoji">
                  👀
                </span>{" "}
                and apply for the interesting projects directly through the app
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Managers see{" "}
                <span style={{ fontWeight: "bold", color: "#f16a70" }}>
                  Job Fit Score
                </span>{" "}
                of each employee calculated based on the skills and experience
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Emploees can search for a team member to share knowledge with
                and get help from
              </CustomListItem>
            </Appear>
          </List>
        </Slide>

        <Slide transition={["fade"]} bgColor="primary" textColor="secondary">
          <Heading size={4} textColor="tertiary" caps>
            Solves problems
          </Heading>
          <List>
            <Appear>
              <CustomListItem>
                Inefficient recruitment process{" "}
                <span role="img" aria-label="emoji">
                  🙉
                </span>
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                <span role="img" aria-label="emoji">
                  🛌
                </span>{" "}
                Long time without project (between projects)
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Managers ask{" "}
                <span role="img" aria-label="emoji">
                  😬
                </span>{" "}
                to join project that do not match the defined skill set
              </CustomListItem>
            </Appear>
          </List>
        </Slide>

        <Slide transition={["spin"]} bgColor="primary" textColor="secondary">
          <Heading size={4} textColor="tertiary" caps>
            Key features
          </Heading>
          <List>
            <Appear>
              <CustomListItem>
                Manager can find employees{" "}
                <span role="img" aria-label="emoji">
                  🕴🕴🕴
                </span>{" "}
                easier by skill set and experience specified in search filters
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Manager can see{" "}
                <span role="img" aria-label="emoji">
                  👁️‍
                </span>{" "}
                and sort by user matchup % calculated by required skill set and
                experience of the employee
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Manager can schedule internal recruitment process (meetings like
                technical interview, on-boarding or others...){" "}
                <span role="img" aria-label="emoji">
                  🤝
                </span>
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Employee can fetch available projects and apply directly for the
                chosen one
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Employee can search other employees by their skillset{" "}
                <span role="img" aria-label="emoji">
                  🦄
                </span>{" "}
                and{" "}
                <span role="img" aria-label="emoji">
                  💪
                </span>
                expertise (knowledge sharing)
              </CustomListItem>
            </Appear>
          </List>
        </Slide>

        <Slide transition={["slide"]} bgColor="primary" textColor="secondary">
          <Heading size={4} textColor="tertiary" caps>
            Value delivered for Adidas and Infosys
          </Heading>
          <List>
            <Appear>
              <CustomListItem>
                More clear and bigger range of candidates for a project
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Target people skills and experience within a team to be able to
                share knowledge and solve problems quicker
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>Standardized procedure ✔ </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Reduction in setup time{" "}
                <span role="img" aria-label="emoji">
                  ⌚
                </span>
              </CustomListItem>
            </Appear>
            <Appear>
              <CustomListItem>
                Faster on-boarding{" "}
                <span role="img" aria-label="emoji">
                  ⏩
                </span>
              </CustomListItem>
            </Appear>
          </List>
        </Slide>

        <Slide transition={["spin"]} bgColor="secondary" textColor="primary">
          <BlockQuote>
            <Cite>Our Solution:</Cite>
            <Quote>
              SEARCH ENGINE FOR ADIDAS PROJECTS AND PROFILES MATCHING APP{" "}
              <span role="img" aria-label="emoji">
                🚀
              </span>
            </Quote>
          </BlockQuote>
        </Slide>

        <Slide transition={["zoom"]} bgColor="primary" textColor="secondary">
          <ParticleField config={config} />
          <Text textColor="secondary">Thank you </Text>
        </Slide>
      </Deck>
    );
  }
}
